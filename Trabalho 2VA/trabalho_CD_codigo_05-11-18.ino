//São definidas as variáveis do Display
#define A 2
#define B 3
#define C 4
#define D 5
#define E 6
#define F 7
#define G 8

//São definidas as variáveis do switch
#define switch1 9
#define switch2 10
#define switch3 11
#define switch4 12


void setup()	{
  
  //São definidos as entradas e as saídas
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(F, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(switch1, INPUT);
  pinMode(switch2, INPUT);
  pinMode(switch3, INPUT);
  pinMode(switch4, INPUT);
  
}

void loop()	{
  
  //Cada Switch é lido e armazenado em uma variável
  int a = digitalRead(switch1);
  int b = digitalRead(switch2);
  int c = digitalRead(switch3);
  int d = digitalRead(switch4);
  
  //São chamadas as funções que verificam se cada ponto do Display deve ser ligado ou não
  onA(a, b, c, d);
  onB(a, b, c, d);
  onC(a, b, c, d);
  onD(a, b, c, d);
  onE(a, b, c, d);
  onF(a, b, c, d);
  onG(a, b, c, d);
  
}   

//Função que verifica se o ponto A do Display deve ser ligado
void onA(int a, int b, int c, int d)	{
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(A, (!b&&!d) || (b&&c) || (a&&!c&&!d) || (a&&!b&&!c) || (!a&&c&&d) || (!a&&b&&d));

}

//Função que verifica se o ponto B do Display deve ser ligado
void onB(int a, int b, int c, int d)	{
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(B, (!a&&!b) || (!b&&!d) || (a&&!c&&d) || (!a&&!c&&!d) || (!a&&c&&d));

}

//Função que verifica se o ponto C do Display deve ser ligado
void onC(int a, int b, int c, int d){
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(C, (!a&&!c) || (a&&!b) || (a&&!c&&d) || (!a&&c&&d) || (!a&&b&&c));

}

//Função que verifica se o ponto D do Display deve ser ligado
void onD(int a, int b, int c, int d){
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(D, (a&&!c) || (!a&&!b&&!d) || (!a&&!b&&c) || (b&&c&&!d) || (a&&!b&&d) || (b&&!c&&d));

}


//Função que verifica se o ponto E do Display deve ser ligado
void onE(int a, int b, int c, int d){
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(E, (!a&&!b&&!d) || (a&&c) || (a&&!b&&!d) || (a&&b&&!c) || (!a&&c&&!d));

}

//Função que verifica se o ponto F do Display deve ser ligado
void onF(int a, int b, int c, int d){
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(F, (!c&&!d) || (!a&&b&&!c) || (b&&c&&!d) || (a&&!b&&d) || (a&&c));

}

//Função que verifica se o ponto G do Display deve ser ligado
void onG(int a, int b, int c, int d){
  
  //Expressão obtida a partir do mapa de karnaugh
  digitalWrite(G, (!a&&b&&!c) || (a&&!b) || (!a&&!b&&c) || (a&&d) || (c&&!d));

}